from django.conf.urls.defaults import *

from django.contrib import admin
admin.autodiscover()

urlpatterns = patterns('',
    (r'^$', 'dorestad-bookings.tickets.views.book'),
    url(r'^betalingen/$', 'dorestad-bookings.tickets.views.payments', name='payments'),
    url(r'^overzicht/$', 'dorestad-bookings.tickets.views.bookings', name='bookings'),
    # Example:
    # (r'^dorestad1493/', include('dorestad1493.foo.urls')),

    # Uncomment the admin/doc line below and add 'django.contrib.admindocs' 
    # to INSTALLED_APPS to enable admin documentation:
    # (r'^admin/doc/', include('django.contrib.admindocs.urls')),

    # Enable the auto admin interface
    (r'^admin/(.*)', admin.site.root),
    url(r'^login/$', 'django.contrib.auth.views.login', {'template_name': 'tickets/login.html'}, name='login'),
    url(r'^logout/$', 'django.contrib.auth.views.logout_then_login', name='logout'),
)
