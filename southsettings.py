# Database settings for when doing a database migration using south,
# which requires more privileges.

import sys
if len(sys.argv) >= 2 and 'manage.py' in sys.argv[0] and sys.argv[1] == 'migrate':
    import south

    try:
        # We cache the prompted values in the south module, since the
        # settings module gets loaded multiple times...
        DATABASE_USER = south.cached_db_user
        DATABASE_PASSWORD = south.cached_db_pw
    except AttributeError:
        DATABASE_USER = raw_input("Privileged database user: ")
        DATABASE_PASSWORD = raw_input("Database password for %s: " % DATABASE_USER)
        south.cached_db_user = DATABASE_USER
        south.cached_db_pw = DATABASE_PASSWORD
