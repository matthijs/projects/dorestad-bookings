from django.contrib import admin
from models import Booking

class BookingAdmin(admin.ModelAdmin):
    list_filter=('show', 'payment')
    search_fields=('name', 'email')
    list_display=('name', 'show', 'tickets', 'created', 'payment')

admin.site.register(Booking, BookingAdmin)
