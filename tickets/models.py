# coding=utf-8

from django.db import models
from django.utils.formats import number_format

# Create your models here.

SHOW_CHOICES = [ ("vrijdag", "vrijdag 17 december om 20:00")
               , ("zaterdag", "zaterdag 18 december om 20:00")
               , ("zondag", "zondag 19 december om 13:00")
               ]

TICKET_PRICE=7.5

TICKETS_CHOICES = [ (n, u"%s (€ %s)" % (n, number_format(n * TICKET_PRICE,2))) for n in range(1, 6)]

class Booking(models.Model):
    created = models.DateTimeField(auto_now_add=True, editable=False, verbose_name="Datum")
    name = models.CharField(max_length=100, verbose_name="Naam")
    email = models.EmailField(verbose_name="Email adres")
    tickets = models.IntegerField(choices=TICKETS_CHOICES, verbose_name="Aantal kaarten")
    show = models.CharField(max_length=50, choices=SHOW_CHOICES, verbose_name="Voorstelling")
    payment = models.DateTimeField(blank=True, null=True, verbose_name="Betaling vewerkt op")

    def _get_price(self):
        return self.tickets * TICKET_PRICE
    price = property(_get_price)

    def __unicode__(self):
        return u'#%s: %s - %s - %s kaart%s (€ %s)' % (
            self.pk, self.name,
            self.get_show_display(),
            self.tickets, "en" if self.tickets != 1 else "",
            number_format(self.price, 2))
