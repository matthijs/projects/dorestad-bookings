# encoding: utf-8
import datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models

class Migration(SchemaMigration):
    
    def forwards(self, orm):
        
        # Deleting field 'Booking.address1'
        db.delete_column('tickets_booking', 'address1')

        # Deleting field 'Booking.address2'
        db.delete_column('tickets_booking', 'address2')
    
    
    def backwards(self, orm):
        
        # Adding field 'Booking.address1'
        db.add_column('tickets_booking', 'address1', self.gf('django.db.models.fields.CharField')(default='', max_length=50), keep_default=False)

        # Adding field 'Booking.address2'
        db.add_column('tickets_booking', 'address2', self.gf('django.db.models.fields.CharField')(default='', max_length=50), keep_default=False)
    
    
    models = {
        'tickets.booking': {
            'Meta': {'object_name': 'Booking'},
            'created': ('django.db.models.fields.DateTimeField', [], {'auto_now_add': 'True', 'blank': 'True'}),
            'email': ('django.db.models.fields.EmailField', [], {'max_length': '75'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'payment': ('django.db.models.fields.DateTimeField', [], {'null': 'True', 'blank': 'True'}),
            'show': ('django.db.models.fields.CharField', [], {'max_length': '50'}),
            'tickets': ('django.db.models.fields.IntegerField', [], {})
        }
    }
    
    complete_apps = ['tickets']
