# encoding: utf-8
import datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models

class Migration(SchemaMigration):
    
    def forwards(self, orm):
        
        # Adding field 'Booking.payment'
        db.add_column('tickets_booking', 'payment', self.gf('django.db.models.fields.DateTimeField')(null=True, blank=True), keep_default=False)
    
    
    def backwards(self, orm):
        
        # Deleting field 'Booking.payment'
        db.delete_column('tickets_booking', 'payment')
    
    
    models = {
        'tickets.booking': {
            'Meta': {'object_name': 'Booking'},
            'address1': ('django.db.models.fields.CharField', [], {'max_length': '50'}),
            'address2': ('django.db.models.fields.CharField', [], {'max_length': '50'}),
            'created': ('django.db.models.fields.DateTimeField', [], {'auto_now_add': 'True', 'blank': 'True'}),
            'email': ('django.db.models.fields.EmailField', [], {'max_length': '75'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'payment': ('django.db.models.fields.DateTimeField', [], {'null': 'True', 'blank': 'True'}),
            'show': ('django.db.models.fields.CharField', [], {'max_length': '50'}),
            'tickets': ('django.db.models.fields.IntegerField', [], {})
        }
    }
    
    complete_apps = ['tickets']
